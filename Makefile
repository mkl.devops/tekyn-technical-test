DOCKER-COMPOSE	= @docker-compose
COMPOSER				= symfony composer
CONSOLE					= symfony console
.DEFAULT_GOAL 	= help

## —— 🐝 The Symfony Makefile 🐝 ———————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?## .*$$)|(^## )' Makefile | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


## —— Composer 🧙‍♂️ ————————————————————————————————————————————————————————————
install: composer.lock ## Install vendors according to the current composer.lock file
	$(COMPOSER) install -n -q

update: composer.json ## Update vendors according to the composer.json file
	$(COMPOSER) update -w

## —— Tests ✅ —————————————————————————————————————————————————————————————————
test-load-fixtures: phpunit.xml*
	$(CONSOLE) --env=test doctrine:database:create --if-not-exists
	$(CONSOLE) --env=test doctrine:schema:update --force
	$(CONSOLE) --env=test doctrine:fixtures:load -n --purge-with-truncate

test: phpunit.xml* ## Launch main functionnal and unit tests
	APP_ENV=test ./vendor/bin/phpunit --stop-on-failure $(c)

test-report: phpunit.xml* ## Launch main functionnal and unit tests
	APP_ENV=test ./vendor/bin/phpunit --stop-on-failure --log-junit report.xml

## —— Coding standards ✨ ——————————————————————————————————————————————————————
psalm: ## Run psalm only
	./vendor/bin/psalm

stan: ## Run PHPStan only
	./vendor/bin/phpstan analyse -l 9 src

cs-fix: ## Run php-cs-fixer and fix the code.
	./vendor/bin/php-cs-fixer fix --allow-risky=yes

cs-dry: ## Run php-cs-fixer and fix the code.
	./vendor/bin/php-cs-fixer fix --dry-run --allow-risky=yes

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
ps: docker-compose.yaml ## ps containers
	$(DOCKER-COMPOSE) ps

up: docker-compose.yaml ## up services for running containers
	$(DOCKER-COMPOSE) up -d $(c)
	$(DOCKER-COMPOSE) ps

build-up: docker-compose.yaml ## up services for running containers
	$(DOCKER-COMPOSE) up -d --build $(c)
	$(DOCKER-COMPOSE) ps

app: docker-compose.yaml ## exec bash command for containers app
	$(DOCKER-COMPOSE) exec app zsh $(c)

database: docker-compose.yaml ## exec bash command for containers app
	$(DOCKER-COMPOSE) exec database mysql -uroot -ppassword main

restart: docker-compose.yaml ## restart containers
	$(DOCKER-COMPOSE) restart $(c)

down: docker-compose.yaml ## down containers
	$(DOCKER-COMPOSE) down $(c)