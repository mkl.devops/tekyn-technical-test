<?php

namespace App\Tests\Controller;

use App\Checker\SearchChecker;
use App\Client\OpenFoodFactsClient;
use App\DataFixtures\ExcludeFixtures;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchControllerTest extends AbstractLoginTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $openFoodfastClientMock = $this->createMock(OpenFoodFactsClient::class);
        $this->client->getContainer()->set(OpenFoodFactsClient::class, $openFoodfastClientMock);
    }

    public function testUnauthorized(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/search');
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    public function testNothingCriteria(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_GET, '/api/search');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testWrongCriteria(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_GET, '/api/search');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testOneCriteria(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_GET, '/api/search', [
            SearchChecker::FIELD_CATEGORIES => 'coffee',
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testTwoCriteria(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_GET, '/api/search', [
            SearchChecker::FIELD_CATEGORIES => 'coffee',
            SearchChecker::FIELD_CRITERIAS => [
                'allergens' => 'soja',
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testCheckExcludedEan(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_GET, '/api/search', [
            SearchChecker::FIELD_CATEGORIES => 'coffee',
            SearchChecker::FIELD_CRITERIAS => [
                'allergens' => 'soja',
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $pattern = sprintf('#%s#', implode('|', ExcludeFixtures::getEanList()));
        $this->assertDoesNotMatchRegularExpression($pattern, $this->client->getResponse()->getContent());
    }
}
