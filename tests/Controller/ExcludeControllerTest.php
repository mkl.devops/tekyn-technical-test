<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExcludeControllerTest extends AbstractLoginTestCase
{
    public function testNotAllowed(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/exclude/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testUnauthorized(): void
    {
        $this->client->request(Request::METHOD_POST, '/api/exclude/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testExcludeEan(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_POST, '/api/exclude/3229820129488');

        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertStringContainsString('"ean":"3229820129488"', $this->client->getResponse()->getContent());
    }

    public function testExcludeAlreadyExists(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_POST, '/api/exclude/3229820129489');
        $this->assertResponseIsSuccessful();

        $this->client->request(Request::METHOD_POST, '/api/exclude/3229820129489');
        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }
}
