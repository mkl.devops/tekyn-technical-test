<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SaveSubstitutionControllerTest extends AbstractLoginTestCase
{
    public function testNotAllowed(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/save/substitution/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testUnauthorized(): void
    {
        $this->client->request(Request::METHOD_POST, '/api/save/substitution/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    public function testSave(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_POST, '/api/save/substitution/3187570015447');
        $this->assertResponseIsSuccessful();
    }
}
