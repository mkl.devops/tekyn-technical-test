<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DeleteControllerTest extends AbstractLoginTestCase
{
    public function testNotAllowed(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/delete/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testUnauthorized(): void
    {
        $this->client->request(Request::METHOD_DELETE, '/api/delete/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testExcludeEanNotFound(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_DELETE, '/api/delete/1313');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testExcludeEan(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_DELETE, '/api/delete/3187570015447');

        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertStringContainsString('"ean":"3187570015447"', $this->client->getResponse()->getContent());
    }
}
