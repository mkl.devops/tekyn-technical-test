<?php

namespace App\Tests\Controller;

use App\Exception\AppException;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractLoginTestCase extends WebTestCase
{
    protected KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        DatabasePrimer::prime($this->client->getKernel());
    }

    public function requestLoginCheck(string $username, string $password): void
    {
        $this->client->request(Request::METHOD_POST, '/api/login_check', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'username' => $username,
            'password' => $password,
        ]));
    }

    public function loginCheck(string $username, string $password): void
    {
        $this->requestLoginCheck($username, $password);
        if (Response::HTTP_OK !== $this->client->getResponse()->getStatusCode()) {
            throw new AppException($this->client->getResponse()->getContent());
        }
        $data = json_decode($this->client->getResponse()->getContent(), true);
        $this->client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));
    }
}
