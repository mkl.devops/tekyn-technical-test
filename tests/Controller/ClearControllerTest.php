<?php

namespace App\Tests\Controller;

use App\Repository\FavouriteRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class ClearControllerTest extends AbstractLoginTestCase
{
    public function testNotAllowed(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/clear');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testUnauthorized(): void
    {
        $this->client->request(Request::METHOD_DELETE, '/api/clear');
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testClear(): void
    {
        $this->loginCheck('admin', 'admin');
        $this->client->request(Request::METHOD_DELETE, '/api/clear');
        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());

        /** @var FavouriteRepository $repository */
        $repository = $this->client->getContainer()->get(FavouriteRepository::class);

        /** @var Security $security */
        $security = $this->client->getContainer()->get(Security::class);
        $this->assertEmpty($repository->findOneBy(['user' => $security->getUser()]));
    }
}
