<?php

namespace App\Tests\Checker;

use App\Checker\SearchChecker;
use App\Exception\AppBadRequestException;
use PHPUnit\Framework\TestCase;

class SearchCheckerTest extends TestCase
{
    public function testCheckFieldRequiredEmpty(): void
    {
        $this->expectException(AppBadRequestException::class);
        SearchChecker::checkFieldRequired([]);
    }

    public function testWrongCheckFieldRequired(): void
    {
        $this->expectException(AppBadRequestException::class);
        SearchChecker::checkFieldRequired(['wrong-fields' => '']);
    }

    public function testOneCheckFieldRequiredEmpty(): void
    {
        $this->expectException(AppBadRequestException::class);
        SearchChecker::checkFieldRequired([SearchChecker::FIELD_PRODUCT_NAME => '']);
    }

    public function testOneCheckFieldRequired(): void
    {
        $query = SearchChecker::checkFieldRequired([SearchChecker::FIELD_CATEGORIES => 'coffee']);
        $this->assertContains('coffee', $query);
    }

    public function testGenerateQueryEmpty(): void
    {
        $query = SearchChecker::generateQuery([]);
        $this->assertEmpty($query);
    }

    public function testGenerateQueryOne(): void
    {
        $query = SearchChecker::generateQuery([SearchChecker::FIELD_CATEGORIES => 'coffee']);
        $this->assertEquals($query, [
            'tagtype_0' => SearchChecker::FIELD_CATEGORIES,
            'tag_contains_0' => SearchChecker::VALUE_CONTAINS,
            'tag_0' => 'coffee',
        ]);
    }

    public function testGenerateQueryWithCriteria(): void
    {
        $query = SearchChecker::generateQuery([
            SearchChecker::FIELD_CATEGORIES => 'coffee',
            SearchChecker::FIELD_CRITERIAS => [
                'allergens' => 'soja',
            ],
        ]);

        $this->assertEquals($query, [
            'tagtype_0' => SearchChecker::FIELD_CATEGORIES,
            'tag_contains_0' => SearchChecker::VALUE_CONTAINS,
            'tag_0' => 'coffee',
            'tagtype_1' => 'allergens',
            'tag_contains_1' => SearchChecker::VALUE_CONTAINS,
            'tag_1' => 'soja',
        ]);
    }
}
