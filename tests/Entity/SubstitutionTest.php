<?php

namespace App\Tests\Entity;

use App\Entity\Substitution;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class SubstitutionTest extends TestCase
{
    public function testUserCreate(): void
    {
        $user = (new User())
            ->setUsername('name')
            ->setPassword('password')
            ->setRoles(['ROLE_ADMIN'])
        ;

        $favourite = (new Substitution())
            ->setEan('3187570015447')
            ->setEanSubstitution('5600380898836')
            ->setUser($user)
        ;

        $this->assertEquals($favourite->getEan(), '3187570015447');
        $this->assertEquals($favourite->getUser(), $user);
    }
}
