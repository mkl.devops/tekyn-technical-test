<?php

namespace App\Tests\Entity;

use App\Entity\Favourite;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class FavouriteTest extends TestCase
{
    public function testUserCreate(): void
    {
        $user = (new User())
            ->setUsername('name')
            ->setPassword('password')
            ->setRoles(['ROLE_ADMIN'])
        ;

        $favourite = (new Favourite())
            ->setEan('3187570015447')
            ->setUser($user)
        ;

        $this->assertEquals($favourite->getEan(), '3187570015447');
        $this->assertEquals($favourite->getUser(), $user);
    }
}
