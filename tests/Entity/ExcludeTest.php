<?php

namespace App\Tests\Entity;

use App\Entity\Exclude;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ExcludeTest extends TestCase
{
    public function testUserCreate(): void
    {
        $user = (new User())
            ->setUsername('name')
            ->setPassword('password')
            ->setRoles(['ROLE_ADMIN'])
        ;

        $exclude = (new Exclude())
            ->setEan('3187570015447')
            ->setUser($user)
        ;

        $this->assertEquals($exclude->getEan(), '3187570015447');
        $this->assertEquals($exclude->getUser(), $user);
    }
}
