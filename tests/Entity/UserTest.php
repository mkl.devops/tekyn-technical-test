<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\UserInterface;

class UserTest extends TestCase
{
    public function testUserInstance(): void
    {
        $user = new User();
        $this->assertInstanceOf(UserInterface::class, $user);
    }

    public function testUserCreate(): void
    {
        $user = (new User())
            ->setUsername('name')
            ->setPassword('password')
            ->setRoles(['ROLE_ADMIN'])
        ;
        $this->assertEquals($user->getUserIdentifier(), 'name');
        $this->assertEquals($user->getPassword(), 'password');
        $this->assertContains('ROLE_USER', $user->getRoles());
        $this->assertContains('ROLE_ADMIN', $user->getRoles());
    }
}
