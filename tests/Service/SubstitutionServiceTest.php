<?php

namespace App\Tests\Service;

use App\Entity\Substitution;
use App\Exception\AppBadRequestException;
use App\Repository\SubstitutionRepository;
use App\Repository\UserRepository;
use App\Service\SubstitutionService;
use App\Service\SubstitutionServiceInterface;
use App\Tests\DatabasePrimer;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SubstitutionServiceTest extends KernelTestCase
{
    private SubstitutionServiceInterface $service;
    private SubstitutionRepository $substitutionRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);

        /** @var UserRepository $userRepository */
        $userRepository = $kernel->getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneBy(['username' => 'admin']);

        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $kernel->getContainer()->get('security.token_storage');
        if (null === $tokenStorage->getToken()) {
            $token = new JWTUserToken(['ROLE_SUPER_ADMIN'], $user);
            $tokenStorage->setToken($token);
        }

        $this->service = $kernel->getContainer()->get(SubstitutionService::class);
        $this->substitutionRepository = $kernel->getContainer()->get(SubstitutionRepository::class);
    }

    public function testSearchEmpty(): void
    {
        $this->expectException(AppBadRequestException::class);
        $this->service->set('1532');
    }

    public function testSet(): void
    {
        $ean = '4104420031500';
        $this->service->set($ean);
        $substitution = $this->substitutionRepository->findOneBy(['ean' => $ean]);
        $this->assertInstanceOf(Substitution::class, $substitution);
    }
}
