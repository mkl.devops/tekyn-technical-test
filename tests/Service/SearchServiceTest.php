<?php

namespace App\Tests\Service;

use App\Checker\SearchChecker;
use App\Exception\AppBadRequestException;
use App\Model\ProductModel;
use App\Service\SearchService;
use App\Service\SearchServiceInterface;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SearchServiceTest extends KernelTestCase
{
    private SearchServiceInterface $service;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);
        $this->service = $kernel->getContainer()->get(SearchService::class);
    }

    public function testSearchEmpty(): void
    {
        $this->expectException(AppBadRequestException::class);
        $this->service->get([]);
    }

    public function testSearchCoffee(): void
    {
        $products = $this->service->get([
            SearchChecker::FIELD_CATEGORIES => 'coffee',
        ]);

        $this->assertContainsOnlyInstancesOf(ProductModel::class, $products);
    }
}
