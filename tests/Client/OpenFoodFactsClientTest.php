<?php

namespace App\Tests\Client;

use App\Checker\SearchChecker;
use App\Client\OpenFoodFactsClient;
use App\Exception\AppBadRequestException;
use App\Model\ProductModel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OpenFoodFactsClientTest extends KernelTestCase
{
    private OpenFoodFactsClient $client;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->client = $kernel->getContainer()->get(OpenFoodFactsClient::class);
    }

    public function testSearchEmptyCriteria(): void
    {
        $result = $this->client->search([]);
        $this->assertContainsOnlyInstancesOf(ProductModel::class, $result);
    }

    public function testSearchOneCriteria(): void
    {
        $result = $this->client->search([SearchChecker::FIELD_CATEGORIES => 'coffee']);
        $this->assertContainsOnlyInstancesOf(ProductModel::class, $result);
    }

    public function testFindNotExists(): void
    {
        $this->expectException(AppBadRequestException::class);
        $this->client->find('23004');
    }

    public function testFind(): void
    {
        $result = $this->client->find('01223004');
        $this->assertInstanceOf(ProductModel::class, $result);
    }

    public function testBestOfcatgoryWrong(): void
    {
        $this->expectException(AppBadRequestException::class);
        $this->client->bestOfcatgory('wrong');
    }

    public function testBestOfcatgory(): void
    {
        $result = $this->client->bestOfcatgory('sweetened-beverages');
        $this->assertInstanceOf(ProductModel::class, $result);
    }
}
