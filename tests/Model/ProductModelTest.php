<?php

namespace App\Tests\Model;

use App\Model\ProductModel;
use PHPUnit\Framework\TestCase;

class ProductModelTest extends TestCase
{
    public function testInstance()
    {
        $product = (new ProductModel())
        ->setName('name')
        ->setNutriScore(10)
        ->setIngredients(['eggs', 'potassium'])
        ->setAllergens('en:gluten')
        ->setNutritionalValues([
            'carbohydrates' => 59,
            'carbohydrates_100g' => 59,
            'carbohydrates_serving' => 59,
            'carbohydrates_unit' => 'g',
        ])
        ->setEan(152301)
        ->setBrands('lu')
        ->setBestSubstitute('test')
        ;

        $this->assertEquals($product->getName(), 'name');
        $this->assertEquals($product->getNutriScore(), 10);
        $this->assertEquals($product->getIngredients(), ['eggs', 'potassium']);
        $this->assertEquals($product->getNutritionalValues(), [
            'carbohydrates' => 59,
            'carbohydrates_100g' => 59,
            'carbohydrates_serving' => 59,
            'carbohydrates_unit' => 'g',
        ]);
        $this->assertEquals($product->getEan(), 152301);
        $this->assertEquals($product->getBrands(), 'lu');
        $this->assertEquals($product->getBestSubstitute(), 'test');
        $this->assertEquals($product->getAllergens(), 'en:gluten');
    }
}
