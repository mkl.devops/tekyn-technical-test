<?php

namespace App\Tests\Repository;

use App\Entity\Exclude;
use App\Repository\ExcludeRepository;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExcludeRepositoryTest extends KernelTestCase
{
    private ExcludeRepository $repository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);
        $this->repository = $kernel->getContainer()->get(ExcludeRepository::class);
    }

    public function testFindAll(): void
    {
        $Excludes = $this->repository->findAll();
        $this->assertContainsOnlyInstancesOf(Exclude::class, $Excludes);
    }
}
