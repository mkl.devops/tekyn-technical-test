<?php

namespace App\Tests\Repository;

use App\Entity\Favourite;
use App\Repository\FavouriteRepository;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FavouriteRepositoryTest extends KernelTestCase
{
    private FavouriteRepository $repository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);
        $this->repository = $kernel->getContainer()->get(FavouriteRepository::class);
    }

    public function testFindAll(): void
    {
        $favourites = $this->repository->findAll();
        $this->assertContainsOnlyInstancesOf(Favourite::class, $favourites);
    }
}
