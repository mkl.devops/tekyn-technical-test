<?php

namespace App\Tests\Repository;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    private UserRepository $repository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);
        $this->repository = $kernel->getContainer()->get(UserRepository::class);
    }

    public function testFindAll(): void
    {
        $users = $this->repository->findAll();
        $this->assertContainsOnlyInstancesOf(User::class, $users);
    }
}
