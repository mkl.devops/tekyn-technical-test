<?php

namespace App\Tests\Repository;

use App\Entity\Substitution;
use App\Repository\SubstitutionRepository;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SubstitutionRepositoryTest extends KernelTestCase
{
    private SubstitutionRepository $repository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);
        $this->repository = $kernel->getContainer()->get(SubstitutionRepository::class);
    }

    public function testFindAll(): void
    {
        $favourites = $this->repository->findAll();
        $this->assertContainsOnlyInstancesOf(Substitution::class, $favourites);
    }
}
