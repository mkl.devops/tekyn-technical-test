<?php

namespace App\Entity;

use App\Repository\FavouriteRepository;
use App\Traits\EanEntityTrait;
use App\Traits\IdEntityTrait;
use App\Traits\TimestampableEntityTrait;
use App\Traits\UserEntityTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FavouriteRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\UniqueConstraint(columns: ['user_id', 'ean'])]
class Favourite
{
    use IdEntityTrait;
    use EanEntityTrait;
    use UserEntityTrait;
    use TimestampableEntityTrait;
}
