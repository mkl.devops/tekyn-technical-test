<?php

namespace App\Entity;

use App\Repository\SubstitutionRepository;
use App\Traits\EanEntityTrait;
use App\Traits\IdEntityTrait;
use App\Traits\TimestampableEntityTrait;
use App\Traits\UserEntityTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SubstitutionRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\UniqueConstraint(columns: ['ean'])]
class Substitution
{
    use IdEntityTrait;
    use EanEntityTrait;
    use UserEntityTrait;
    use TimestampableEntityTrait;

    #[ORM\Column(type: Types::STRING, length: 30, nullable: false)]
    private ?string $eanSubstitution = null;

    public function getEanSubstitution(): ?string
    {
        return $this->eanSubstitution;
    }

    public function setEanSubstitution(string $eanSubstitution): self
    {
        $this->eanSubstitution = $eanSubstitution;

        return $this;
    }
}
