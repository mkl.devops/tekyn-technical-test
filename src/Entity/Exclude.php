<?php

namespace App\Entity;

use App\Repository\ExcludeRepository;
use App\Traits\EanEntityTrait;
use App\Traits\IdEntityTrait;
use App\Traits\TimestampableEntityTrait;
use App\Traits\UserEntityTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExcludeRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\UniqueConstraint(columns: ['user_id', 'ean'])]
class Exclude
{
    use IdEntityTrait;
    use EanEntityTrait;
    use UserEntityTrait;
    use TimestampableEntityTrait;
}
