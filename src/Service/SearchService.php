<?php

namespace App\Service;

use App\Checker\SearchChecker;
use App\Client\OpenFoodFactsClientInterface;
use App\Entity\Exclude;
use App\Model\ProductModel;
use App\Repository\ExcludeRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class SearchService implements SearchServiceInterface
{
    public function __construct(
        private LoggerInterface $logger,
        private OpenFoodFactsClientInterface $client,
        private ExcludeRepository $excludeRepository,
        private SubstitutionService $substitutionService,
        private Security $security,
    ) {
    }

    /**
     * @param string[] $query
     *
     * @return ProductModel[]
     */
    public function get(array $query): array
    {
        $query = SearchChecker::checkFieldRequired($query);
        $eanExcluded = $this->getEanExcluded();
        $this->logger->info('Query field required checked', compact('query', 'eanExcluded'));
        $products = $this->client->search($query, $eanExcluded);

        $products = $this->substitutionService->findSubstitution($products);
        $this->logger->info('result of product with substitute', compact('query', 'eanExcluded'));

        return $products;
    }

    /**
     * @return string[]
     */
    public function getEanExcluded(): array
    {
        $result = $this->excludeRepository->findBy(['user' => $this->security->getUser()]);

        return array_map(static fn (Exclude $exclude) => (string) $exclude->getEan(), $result);
    }
}
