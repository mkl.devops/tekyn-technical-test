<?php

namespace App\Service;

use App\Model\ProductModel;

interface SubstitutionServiceInterface
{
    public function set(string $ean): ProductModel;

    /**
     * @param ProductModel[] $products
     *
     * @return ProductModel[]
     */
    public function findSubstitution(array $products): array;
}
