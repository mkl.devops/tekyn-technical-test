<?php

namespace App\Service;

use App\Model\ProductModel;

interface SearchServiceInterface
{
    /**
     * @param string[] $queries
     *
     * @return ProductModel[]
     */
    public function get(array $queries): array;

    /**
     * @return string[]
     */
    public function getEanExcluded(): array;
}
