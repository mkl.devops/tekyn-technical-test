<?php

namespace App\Service;

use App\Client\OpenFoodFactsClientInterface;
use App\Entity\Substitution;
use App\Entity\User;
use App\Exception\AppBadRequestException;
use App\Model\ProductModel;
use App\Repository\SubstitutionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class SubstitutionService implements SubstitutionServiceInterface
{
    public function __construct(
        private LoggerInterface $logger,
        private OpenFoodFactsClientInterface $client,
        private SubstitutionRepository $substitutionRepository,
        private EntityManagerInterface $entityManager,
        private Security $security
    ) {
    }

    public function set(string $ean): ProductModel
    {
        $product = $this->client->find($ean);
        if (!$category = $product->getComparedToCategory()) {
            throw new AppBadRequestException('Not found category with ean '.$ean);
        }

        $productSubstitution = $this->client->bestOfcatgory($category);
        $product->setBestSubstitute($product->getEan());
        $substitution = $this->substitutionRepository->findOneBy(['ean' => $ean]);

        $this->logger->debug('substition found products', compact('product', 'productSubstitution'));

        if (!$substitution instanceof Substitution) {
            $substitution = (new Substitution())
                ->setEan($ean);
        }

        /** @var User $user */
        $user = $this->security->getUser();
        $substitution->setEanSubstitution((string) $productSubstitution->getEan())
            ->setUser($user);

        $this->entityManager->persist($substitution);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param ProductModel[] $products
     *
     * @return ProductModel[]
     */
    public function findSubstitution(array $products): array
    {
        $eanList = array_map(static fn (ProductModel $product) => (string) $product->getEan(), $products);
        $substitutions = $this->substitutionRepository->findBy([
            'user' => $this->security->getUser(),
            'ean' => $eanList,
        ]);

        // with substitutions found, we'll set best Substitute on ProductModel corresponding
        return array_map(static function (ProductModel $product) use ($substitutions) {
            // filter the current ean on list substitution
            [$substitution] = array_filter(
                $substitutions,
                static fn (Substitution $substitution) => $substitution->getEan() === $product->getEan()
            ) ?: [null];

            if (null !== $substitution) {
                $product->setBestSubstitute($substitution->getEanSubstitution());
            }

            return $product;
        }, $products);
    }
}
