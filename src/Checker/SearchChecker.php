<?php

namespace App\Checker;

use App\Exception\AppBadRequestException;

class SearchChecker
{
    public const FIELD_CODE = 'code';
    public const FIELD_BRANDS = 'brands';
    public const FIELD_PRODUCT_NAME = 'product_name';
    public const FIELD_CATEGORIES = 'categories';
    public const FIELD_CRITERIAS = 'criterias';
    public const VALUE_CONTAINS = 'contains';
    public const VALUE_NO_CONTAINS = 'does_not_contain';

    private static int $i = 0;

    /**
     * @return string[]
     */
    private static function fields(): array
    {
        return [
            self::FIELD_PRODUCT_NAME,
            self::FIELD_CRITERIAS,
            self::FIELD_CODE,
            self::FIELD_BRANDS,
            self::FIELD_CATEGORIES,
        ];
    }

    /**
     * @param string[] $queries
     *
     * @return string[]
     */
    public static function checkFieldRequired(array $queries): array
    {
        // filter with field defined and not empty value
        $filtered = array_filter($queries, function (null|string|int|array $v, string $k) {
            return in_array($k, self::fields()) && !empty($v);
        }, ARRAY_FILTER_USE_BOTH);

        if (empty($filtered)) {
            throw new AppBadRequestException(sprintf('Use one or more criterias (%s)', implode(', ', self::fields())));
        }

        return $filtered;
    }

    /**
     * @param string[] $filtered
     * @param string[] $eanExclded
     *
     * @return string[]
     */
    public static function generateQuery(array $filtered, array $eanExclded = []): array
    {
        /** @var string[] $criterias */
        $criterias = $filtered[self::FIELD_CRITERIAS] ?? [];
        $filtered = array_filter($filtered, static fn (string $k): bool => self::FIELD_CRITERIAS !== $k, ARRAY_FILTER_USE_KEY);
        self::$i = 0;
        $filteredQuery = self::applyQuerySearch($filtered);
        $filteredQuery += self::applyQuerySearch($criterias);
        $filteredQuery += self::applyQuerySearch($eanExclded, self::FIELD_CODE, false);

        return $filteredQuery;
    }

    /**
     * @param string[] $array
     *
     * @return string[]
     */
    private static function applyQuerySearch(array $array, string $key = null, bool $contains = true): array
    {
        $query = [];
        foreach ($array as $k => $v) {
            $query += [
                'tagtype_'.self::$i => $key ?? $k,
                'tag_contains_'.self::$i => $contains ? self::VALUE_CONTAINS : self::VALUE_NO_CONTAINS,
                'tag_'.self::$i => $v,
            ];
            ++self::$i;
        }

        return $query;
    }
}
