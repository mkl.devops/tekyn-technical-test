<?php

namespace App\Model;

use Symfony\Component\Serializer\Annotation\Ignore;

class ProductModel
{
    #[Ignore]
    private ?string $comparedToCategory = null;

    /**
     * @param string[] $ingredients
     * @param string[] $nutritionalValues
     */
    public function __construct(
        private ?string $name = null,
        private ?string $ean = null,
        private ?string $brands = null,
        private array $ingredients = [],
        private ?string $allergens = null,
        private ?string $nutriScore = null,
        private array $nutritionalValues = [],
        private ?string $bestSubstitute = null,
    ) {
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEan(): ?string
    {
        return $this->ean;
    }

    public function setEan(?string $ean): self
    {
        $this->ean = $ean;

        return $this;
    }

    public function getBrands(): ?string
    {
        return $this->brands;
    }

    public function setBrands(?string $brands): self
    {
        $this->brands = $brands;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    /**
     * @param string[] $ingredients
     */
    public function setIngredients(array $ingredients): self
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    public function getNutriScore(): ?string
    {
        return $this->nutriScore;
    }

    public function setNutriScore(?string $nutriScore): self
    {
        $this->nutriScore = $nutriScore;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getNutritionalValues(): array
    {
        return $this->nutritionalValues;
    }

    /**
     * @param string[] $nutritionalValues
     */
    public function setNutritionalValues(array $nutritionalValues): self
    {
        $this->nutritionalValues = $nutritionalValues;

        return $this;
    }

    public function getBestSubstitute(): ?string
    {
        return $this->bestSubstitute;
    }

    public function setBestSubstitute(?string $bestSubstitute): self
    {
        $this->bestSubstitute = $bestSubstitute;

        return $this;
    }

    public function getAllergens(): ?string
    {
        return $this->allergens;
    }

    public function setAllergens(string $allergens): self
    {
        $this->allergens = $allergens;

        return $this;
    }

    public function getComparedToCategory(): ?string
    {
        return $this->comparedToCategory;
    }

    public function setComparedToCategory(?string $comparedToCategory): self
    {
        $this->comparedToCategory = $comparedToCategory;

        return $this;
    }
}
