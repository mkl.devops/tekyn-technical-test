<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class AppBadRequestException extends AppException
{
    public function __construct(
        string $message = '',
        int $code = 0,
        \Throwable $previous = null,
        array $headers = []
    ) {
        parent::__construct($message, $code, $previous, statusCode: Response::HTTP_BAD_REQUEST, headers: $headers);
    }
}
