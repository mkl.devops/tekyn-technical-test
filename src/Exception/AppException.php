<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class AppException extends \Exception implements HttpExceptionInterface
{
    /**
     * @param string[] $headers
     */
    public function __construct(
        string $message = '',
        int $code = 0,
        \Throwable $previous = null,
        protected int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        protected array $headers = []
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString(): string
    {
        return $this->getMessage();
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return string[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string[] $headers
     */
    public function setHeaders(array $headers): static
    {
        $this->headers = $headers;

        return $this;
    }
}
