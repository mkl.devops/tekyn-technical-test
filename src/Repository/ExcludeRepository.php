<?php

namespace App\Repository;

use App\Entity\Exclude;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Exclude|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exclude|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exclude[]    findAll()
 * @method Exclude[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExcludeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exclude::class);
    }
}
