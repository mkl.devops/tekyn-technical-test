<?php

namespace App\Repository;

use App\Entity\Substitution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Substitution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Substitution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Substitution[]    findAll()
 * @method Substitution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstitutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Substitution::class);
    }
}
