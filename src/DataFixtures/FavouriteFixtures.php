<?php

namespace App\DataFixtures;

use App\Entity\Favourite;
use App\Entity\User;
use App\Exception\AppException;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FavouriteFixtures extends Fixture implements OrderedFixtureInterface, AppFixturesInterface
{
    public const EAN_3187570015447 = '3187570015447';
    public const EAN_3033710071005 = '3033710071005';
    public const EAN_7613032568436 = '7613032568436';
    public const EAN_3033710075225 = '3033710075225';

    public function load(ObjectManager $manager): void
    {
        if (!($user = $this->getReference(UserFixtures::USER_ADMIN)) instanceof User) {
            throw new AppException('Error get reference of User admin');
        }

        foreach (self::getEanList() as $ean) {
            $favourite = (new Favourite())
                ->setEan($ean)
                ->setUser($user)
            ;

            $manager->persist($favourite);
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getEanList(): array
    {
        return [
            self::EAN_3033710071005,
            self::EAN_3033710075225,
            self::EAN_3187570015447,
            self::EAN_7613032568436,
        ];
    }

    public function getOrder()
    {
        return 2;
    }
}
