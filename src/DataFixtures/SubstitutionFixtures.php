<?php

namespace App\DataFixtures;

use App\Entity\Substitution;
use App\Entity\User;
use App\Exception\AppException;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SubstitutionFixtures extends Fixture implements OrderedFixtureInterface, AppFixturesInterface
{
    public const EAN_3033710065066 = '3033710065066';
    public const EAN_0060383985905 = '0060383985905';
    public const EAN_4006040048343 = '4006040048343';
    public const EAN_6130632000827 = '6130632000827';
    public const SBT_8715700407760 = '8715700407760';

    public function load(ObjectManager $manager): void
    {
        if (!($user = $this->getReference(UserFixtures::USER_ADMIN)) instanceof User) {
            throw new AppException('Error get reference of User admin');
        }

        foreach (self::getEanList() as $ean) {
            $favourite = (new Substitution())
                ->setEan($ean)
                ->setEanSubstitution(self::SBT_8715700407760)
                ->setUser($user)
            ;

            $manager->persist($favourite);
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getEanList(): array
    {
        return [
            self::EAN_3033710065066,
            self::EAN_0060383985905,
            self::EAN_4006040048343,
            self::EAN_6130632000827,
        ];
    }

    public function getOrder()
    {
        return 2;
    }
}
