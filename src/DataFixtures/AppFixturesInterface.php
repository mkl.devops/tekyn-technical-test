<?php

namespace App\DataFixtures;

interface AppFixturesInterface
{
    /**
     * @return string[]
     */
    public static function getEanList(): array;
}
