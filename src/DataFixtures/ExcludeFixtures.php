<?php

namespace App\DataFixtures;

use App\Entity\Exclude;
use App\Entity\User;
use App\Exception\AppException;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ExcludeFixtures extends Fixture implements OrderedFixtureInterface, AppFixturesInterface
{
    public const EAN_3033710065066 = '3033710065066';
    public const EAN_3227290000146 = '3227290000146';
    public const EAN_7613034626844 = '7613034626844';
    public const EAN_3033710084913 = '3033710084913';

    public function load(ObjectManager $manager): void
    {
        if (!($user = $this->getReference(UserFixtures::USER_ADMIN)) instanceof User) {
            throw new AppException('Error get reference of User admin');
        }

        foreach (self::getEanList() as $ean) {
            $exclude = (new Exclude())
                ->setEan($ean)
                ->setUser($user)
            ;

            $manager->persist($exclude);
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getEanList(): array
    {
        return [
            self::EAN_3033710065066,
            self::EAN_3227290000146,
            self::EAN_7613034626844,
            self::EAN_3033710084913,
        ];
    }

    public function getOrder()
    {
        return 2;
    }
}
