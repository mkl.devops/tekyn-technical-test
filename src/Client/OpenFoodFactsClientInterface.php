<?php

namespace App\Client;

use App\Model\ProductModel;

interface OpenFoodFactsClientInterface
{
    /**
     * @param string[] $filters
     * @param string[] $eanExcluded
     *
     * @return ProductModel[]
     */
    public function search(array $filters, array $eanExcluded): array;

    public function find(string $ean): ProductModel;

    public function bestOfcatgory(string $category): ProductModel;
}
