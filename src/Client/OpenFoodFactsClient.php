<?php

namespace App\Client;

use App\Checker\SearchChecker;
use App\Exception\AppBadRequestException;
use App\Model\ProductModel;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenFoodFactsClient implements OpenFoodFactsClientInterface
{
    public function __construct(
        private LoggerInterface $logger,
        private HttpClientInterface $client,
        private string $urlOpenFoodFact
    ) {
    }

    /**
     * @param string[] $filters
     * @param string[] $eanExcluded
     *
     * @return ProductModel[]
     */
    public function search(array $filters, array $eanExcluded = []): array
    {
        $query = ['action' => 'process', 'json' => 'true', 'page_size' => 20, 'page' => 1];
        $query += SearchChecker::generateQuery($filters, $eanExcluded);

        $response = $this->client->request(Request::METHOD_GET, $this->urlOpenFoodFact.'/cgi/search.pl', [
            'query' => $query,
            'headers' => ['accept' => 'application/json'],
        ]);
        $this->logger->debug('Response of request search', compact('response'));

        $data = $response->toArray();

        if (empty($data['products'])) {
            throw new AppBadRequestException('Not found products on your search');
        }

        $products = [];
        ['products' => $responseProducts] = $data;
        foreach ($responseProducts as $item) {
            $products[] = self::hydrateProductModel($item);
        }

        return $products;
    }

    public function find(string $ean): ProductModel
    {
        $url = $this->urlOpenFoodFact.'/api/v0/product/'.$ean;
        $this->logger->debug('url of product', compact('url'));
        $response = $this->client->request(Request::METHOD_GET, $url, [
            'headers' => ['accept' => 'application/json'],
        ]);

        $item = $response->toArray();

        if (isset($item['status'])
            && (0 === $item['status'] || 'product not found' === $item['status_verbose'])) {
            throw new AppBadRequestException(sprintf('Product ean %s not found', (string) $item['code']));
        }

        return self::hydrateProductModel($item['product']);
    }

    public function bestOfcatgory(string $category): ProductModel
    {
        $url = $this->urlOpenFoodFact.'/categorie/'.$category;
        $this->logger->debug('url of product', compact('url'));
        $response = $this->client->request(Request::METHOD_GET, $url, [
            'query' => ['json' => true, 'sort_by' => 'nutriscore_score', 'page_size' => 1],
            'headers' => ['accept' => 'application/json'],
        ]);
        $data = $response->toArray();

        if (empty($data['products'])) {
            throw new AppBadRequestException('Not found products on categeory '.$category);
        }

        ['products' => [$item]] = $data;

        return self::hydrateProductModel($item);
    }

    /**
     * @param string[] $item
     */
    private static function hydrateProductModel(array $item): ProductModel
    {
        $comparedToCategory = preg_replace('#(.*:)?(.*)#', '$2', $item['compared_to_category']);

        /** @var string[] $ingredients */
        $ingredients = $item['ingredients'] ?? [];

        /** @var string[] $nutriments */
        $nutriments = $item['nutriments'] ?? [];

        return (new ProductModel())
            ->setBrands((string) $item['brands'])
            ->setName((string) ($item['product_name'] ?? $item['product_name_fr']))
            ->setNutriScore($item['nutriscore_grade'] ?? null)
            ->setIngredients($ingredients)
            ->setAllergens((string) $item['allergens'])
            ->setNutritionalValues($nutriments)
            ->setEan((string) $item['code'])
            ->setComparedToCategory($comparedToCategory)
        ;
    }
}
