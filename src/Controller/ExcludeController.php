<?php

namespace App\Controller;

use App\Entity\Exclude;
use App\Entity\User;
use App\Exception\AppBadRequestException;
use App\Exception\AppException;
use App\Repository\ExcludeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security as NelmioSecurity;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route('/exclude')]
class ExcludeController extends AbstractController
{
    #[Route('/{ean}', name: 'exclude', methods: [Request::METHOD_POST])]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Returns exclude",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=Exclude::class, groups={"full"})))
     * )
     * @OA\Parameter(name="ean", in="path", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Tag(name="Exclude")
     * @NelmioSecurity(name="Bearer")
     */
    public function index(
        string $ean,
        EntityManagerInterface $entityManager,
        ExcludeRepository $repository,
        Security $security
    ): Response {
        try {
            if (!($user = $security->getUser()) instanceof User) {
                throw new AppException('User incompatible to save in exclude');
            }

            if (null !== $repository->findOneBy(['ean' => $ean, 'user' => $user])) {
                throw new AppBadRequestException('You have already saved this ean on your excludes');
            }

            $exclude = (new Exclude())
                ->setEan($ean)
                ->setUser($user);

            $entityManager->persist($exclude);
            $entityManager->flush();

            return $this->json($exclude);
        } catch (ORMException $exception) {
            throw new AppException(message: sprintf('Save exclude ean %d failed', $ean), previous: $exception);
        }
    }
}
