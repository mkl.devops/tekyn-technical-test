<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\AppException;
use App\Repository\FavouriteRepository;
use Doctrine\ORM\Exception\ORMException;
use Nelmio\ApiDocBundle\Annotation\Security as NelmioSecurity;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ClearController extends AbstractController
{
    #[Route('/clear', name: 'clear', methods: [Request::METHOD_DELETE])]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Clear favourite",
     *     @OA\JsonContent(type="object")
     * )
     * @OA\Parameter(name="ean", in="path", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Tag(name="Delete")
     * @NelmioSecurity(name="Bearer")
     */
    public function index(FavouriteRepository $favouriteRepository, Security $security): Response
    {
        try {
            if (!($user = $security->getUser()) instanceof User) {
                throw new AppException('User incompatible to save in exclude');
            }

            $favouriteRepository->clearByUser($user);

            return $this->json([
                'message' => 'favourite clear successfully',
            ]);
        } catch (ORMException $exception) {
            throw new AppException(message: 'Clear favourite failed', previous: $exception);
        }
    }
}
