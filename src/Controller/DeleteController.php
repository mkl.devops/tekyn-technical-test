<?php

namespace App\Controller;

use App\Exception\AppException;
use App\Exception\AppNotFoundException;
use App\Repository\FavouriteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Nelmio\ApiDocBundle\Annotation\Security as NelmioSecurity;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DeleteController extends AbstractController
{
    #[Route('/delete/{ean}', name: 'delete', methods: [Request::METHOD_DELETE])]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Delete favourite",
     *     @OA\JsonContent(type="object")
     * )
     * @OA\Parameter(name="ean", in="path", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Tag(name="Delete")
     * @NelmioSecurity(name="Bearer")
     */
    public function index(
        string $ean,
        FavouriteRepository $favouriteRepository,
        EntityManagerInterface $entityManager,
        Security $security
    ): Response {
        try {
            $favourite = $favouriteRepository->findOneBy(['ean' => $ean, 'user' => $security->getUser()]);
            if (null === $favourite) {
                throw new AppNotFoundException('Not found ean on your favourite');
            }

            $entityManager->remove($favourite);
            $entityManager->flush();

            return $this->json([
                'ean' => $ean,
                'message' => 'favourite deleted successfully',
            ]);
        } catch (ORMException $exception) {
            throw new AppException(message: sprintf('Delete favourite ean %d failed', $ean), previous: $exception);
        }
    }
}
