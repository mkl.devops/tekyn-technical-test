<?php

namespace App\Controller;

use App\Model\ProductModel;
use App\Service\SubstitutionService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/save/substitution')]
class SaveSubstitutionController extends AbstractController
{
    #[Route(
        path: '/{ean}',
        name: 'save_substitution',
        methods: [Request::METHOD_POST],
        requirements: ['ean' => '\d+', 'eanSubstitution' => '\d+']
    )]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Returns substitution saved",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=ProductModel::class, groups={"full"})))
     * )
     * @OA\Parameter(name="ean", in="path", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Tag(name="Save")
     * @Security(name="Bearer")
     */
    public function index(string $ean, SubstitutionService $service): Response
    {
        $product = $service->set($ean);

        return $this->json($product);
    }
}
