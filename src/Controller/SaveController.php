<?php

namespace App\Controller;

use App\Entity\Favourite;
use App\Entity\User;
use App\Exception\AppBadRequestException;
use App\Exception\AppException;
use App\Repository\FavouriteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security as NelmioSecurity;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route('/save')]
class SaveController extends AbstractController
{
    #[Route('/{ean}', name: 'save', methods: [Request::METHOD_POST])]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Returns favourite",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=Favourite::class, groups={"full"})))
     * )
     * @OA\Parameter(name="ean", in="path", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Tag(name="Save")
     * @NelmioSecurity(name="Bearer")
     */
    public function index(
        string $ean,
        EntityManagerInterface $entityManager,
        FavouriteRepository $repository,
        Security $security
    ): Response {
        try {
            if (!($user = $security->getUser()) instanceof User) {
                throw new AppException('User incompatible to save in favourite');
            }

            if (null !== $repository->findOneBy(['ean' => $ean, 'user' => $user])) {
                throw new AppBadRequestException('You have already saved this ean on your favourites');
            }

            $favourite = (new Favourite())
                ->setEan($ean)
                ->setUser($user);

            $entityManager->persist($favourite);
            $entityManager->flush();
        } catch (ORMException $exception) {
            throw new AppException(message: sprintf('Save favourite ean %d failed', $ean), previous: $exception);
        }

        return $this->json($favourite);
    }
}
