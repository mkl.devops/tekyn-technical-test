<?php

namespace App\Controller;

use App\Model\ProductModel;
use App\Service\SearchServiceInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'search', methods: Request::METHOD_GET)]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Returns products of search",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=ProductModel::class, groups={"full"})))
     * )
     * @OA\Parameter(name="categories", in="query", description="The field used to order rewards", @OA\Schema(type="string"))
     * @OA\Parameter(name="brands", in="query", description="The field used to order rewards", @OA\Schema(type="string"))
     * @OA\Parameter(name="code", in="query", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Parameter(name="product_name", in="query", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Parameter(name="criterias", in="query", description="The field EAN of products", @OA\Schema(type="string"))
     * @OA\Tag(name="Search")
     * @Security(name="Bearer")
     */
    public function index(Request $request, SearchServiceInterface $search): JsonResponse
    {
        $data = $search->get($request->query->all());

        return $this->json($data);
    }
}
