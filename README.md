# TEKYN Technical Test

## Run Locally
---
### clone the project
```bash
git clone https://gitlab.com/mkl.devops/tekyn-technical-test.git
```

### Install & run container
```bash
docker-compose up -d

# if you need to build
docker-compose build
```

You can see on http://localhost:801

### Log into the PHP container with zsh
```bash
docker-compose exec app zsh
```
## Data & testing
---
### load fixtures
```bash
docker-compose exec app php bin/console doctrine:fixtures:load
```

### Run unit test
```bash
docker-compose exec app make test
```

### Connect database
```bash
docker-compose exec mysql -uroot -proot main
```
---
## Using
### Get Token
```bash
curl -X POST -H "Content-Type: application/json" http://localhost:801/api/login_check -d '{"username":"admin","password":"admin"}'
```

Go to api doc http://localhost:801/api/doc